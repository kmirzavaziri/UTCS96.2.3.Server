from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)  # main server application. held as app here

# these lines config the flask-SQLAlchemy database
db_path = os.path.join(os.path.dirname(__file__), 'game.db')
db_uri = 'sqlite:///{}'.format(db_path)
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
db = SQLAlchemy(app)

