from server import db
import json


# sql database class to keep web-server users

class user(db.Model):

    username = db.Column(db.String(30), unique=True, nullable=False,
                         primary_key=True)
    password = db.Column(db.String(30), nullable=False)
    score = db.Column(db.Integer)

    def __repr__(self):
        return json.JSONEncoder().encode({'username': self.username,
                'password': self.password, 'score': self.score})


# sql database class to keep game rooms

class room(db.Model):

    room_name = db.Column(db.String(30), unique=True, nullable=False,
                          primary_key=True)
    game_type = db.Column(db.String(10), nullable=False)
    owner = db.Column(db.String(30), nullable=False)
    status = db.Column(db.String(600), nullable=False)
    players = db.Column(db.String(300), nullable=False)

    def __repr__(self):
        return json.JSONEncoder().encode({
            'room_name': self.room_name,
            'game_type': self.game_type,
            'owner': self.owner,
            'status': self.status,
            'players': self.players,
            })

