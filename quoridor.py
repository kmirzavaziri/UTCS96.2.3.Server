#!/usr/bin/python
# -*- coding: utf-8 -*-
import tables
import json
from server import db


class GameRoom:

    def __init__(self, room_name, new=False):  # if new is True it means the room does not exist in sql database
        self.room_name = room_name
        if not new:
            try:
                data = json.loads(self.sql_get_json())
                self.table = data['table']  # table is the Game Board
                self.pawns_pos = data['pawns_pos']  # pawns_pos stores the position of pawns
                self.players_walls = data['players_walls']  # players_walls stores the number of remaining walls for each player
                self.players_goal = data['players_goal']  # players_goal stores the goal squares for each player
                self.players_name = data['players_name']  # players_name stores the nickname of players A, B, C and D
                self.state = data['state']  # state represents the current state of room
                self.players_qty = data['players_qty']  # possible number of players in room according to game type (can be 2 or 4)
            except:
                pass

    # SQL Functions

    def sql_add(self, game_type, owner):  # this function adds a new room to sql database
        if not self.room_name:
            return 'Please provide a room name.'

        if not game_type or game_type != 'q2' and game_type != 'q4':
            return 'Please provide a correct game type.'

        rooms = \
            tables.room.query.filter_by(room_name=self.room_name).first()
        if rooms is not None:
            return 'This room name already exists. Try another.'

        room = tables.room(room_name=self.room_name,
                           game_type=game_type, owner=owner, status='',
                           players='')
        db.session.add(room)
        db.session.commit()
        if room.game_type == 'q2':
            self.players_qty = 2
        elif room.game_type == 'q4':
            self.players_qty = 4
        self.init_room()
        return '1'

    def sql_del(self, current_user):  # this function removes the specified room from sql database
        room = \
            tables.room.query.filter_by(room_name=self.room_name).first()
        if room is None:
            return 'Room not found.'
        if room.owner != current_user:
            return 'Permission Denied!'
        db.session.delete(room)
        db.session.commit()
        return '1'

    def sql_update(self):  # this function updates the sql database with room properties
        room = \
            tables.room.query.filter_by(room_name=self.room_name).first()
        data = {}
        data['table'] = self.table
        data['pawns_pos'] = self.pawns_pos
        data['players_walls'] = self.players_walls
        data['players_goal'] = self.players_goal
        data['players_name'] = self.players_name
        data['state'] = self.state
        room.status = json.JSONEncoder().encode(data)
        db.session.commit()

    def sql_get_json(self):  # this function gets and returns room properties from sql database as a json string
        room = \
            tables.room.query.filter_by(room_name=self.room_name).first()
        room_status = json.loads(room.status)
        if room.game_type == 'q2':
            room_status['players_qty'] = 2
        elif room.game_type == 'q4':
            room_status['players_qty'] = 4
        return json.JSONEncoder().encode(room_status)

    def sql_join(self, player):  # this function adds the inputed player to room
        room = \
            tables.room.query.filter_by(room_name=self.room_name).first()
        if room.players == '':
            players = []
        else:
            players = room.players.split(',')

        if len(players) >= 2 and room.game_type == 'q2':
            return 'The room is full.'
        if len(players) >= 4 and room.game_type == 'q4':
            return 'The room is full.'
        if player in players:
            return '1'

        players.append(player)
        room.players = ','.join(players)
        db.session.commit()

        if self.players_qty == 2:
            for i in 'AB':
                if self.players_name[i] is None:
                    self.players_name[i] = player
                    break
        if self.players_qty == 4:
            for i in 'ABCD':
                if self.players_name[i] is None:
                    self.players_name[i] = player
                    break

        self.sql_update()
        return '1'

    def sql_leave(self, player):  # this function removes the inputed player from room
        room = \
            tables.room.query.filter_by(room_name=self.room_name).first()
        if room.players == '':
            players = []
        else:
            players = room.players.split(',')

        try:
            players.remove(player)
        except:
            pass
        room.players = ','.join(players)
        db.session.commit()

        if self.players_qty == 2:
            for i in 'AB':
                if self.players_name[i] == player:
                    self.players_name[i] = None
        if self.players_qty == 4:
            for i in 'ABCD':
                if self.players_name[i] == player:
                    self.players_name[i] = None

        self.sql_update()
        return '1'

    # Translation Functions

    def trans_room_state(self, state):  # this function translate the state code to meaningful sentence
        if state == 'At':
            return self.players_name['A'] + "'s turn"
        if state == 'Bt':
            return self.players_name['B'] + "'s turn"
        if state == 'Ct':
            return self.players_name['C'] + "'s turn"
        if state == 'Dt':
            return self.players_name['D'] + "'s turn"
        if state == 'Aw':
            return self.players_name['A'] + ' won!'
        if state == 'Bw':
            return self.players_name['B'] + ' won!'
        if state == 'Cw':
            return self.players_name['C'] + ' won!'
        if state == 'Dw':
            return self.players_name['D'] + ' won!'

    def real_to_player(self, player_name):
        player = ''
        if player_name == self.players_name['A']:
            player = 'A'
        elif player_name == self.players_name['B']:
            player = 'B'
        elif player_name == self.players_name['C']:
            player = 'C'
        elif player_name == self.players_name['D']:
            player = 'D'
        return player

    # Game Functions

    def init_room(self):  # this function is used for setting initial values
        self.table = []
        for i in range(17):
            self.table.append([])
            for j in range(17):
                if i % 2 == 0 and j % 2 == 0:
                    self.table[-1].append('O')  # Empty square
                else:
                    self.table[-1].append('O')  # Empty wall place
        self.pawns_pos = {}
        self.players_walls = {}
        self.players_goal = {}
        self.state = 'At'
        if self.players_qty == 2:
            self.table[0][8] = 'A'
            self.table[16][8] = 'B'
            self.pawns_pos['A'] = (0, 8)
            self.pawns_pos['B'] = (16, 8)
            self.players_walls['A'] = 10
            self.players_walls['B'] = 10
            self.players_goal['A'] = (16, -1)
            self.players_goal['B'] = (0, -1)
            self.players_name = {'A': None, 'B': None}
        elif self.players_qty == 4:
            self.table[0][8] = 'A'
            self.table[8][16] = 'B'
            self.table[16][8] = 'C'
            self.table[8][0] = 'D'
            self.pawns_pos['A'] = (0, 8)
            self.pawns_pos['B'] = (8, 16)
            self.pawns_pos['C'] = (16, 8)
            self.pawns_pos['D'] = (8, 0)
            self.players_walls['A'] = 5
            self.players_walls['B'] = 5
            self.players_walls['C'] = 5
            self.players_walls['D'] = 5
            self.players_goal['A'] = (16, -1)
            self.players_goal['B'] = (-1, 0)
            self.players_goal['C'] = (0, -1)
            self.players_goal['D'] = (-1, 16)
            self.players_name = {
                'A': None,
                'B': None,
                'C': None,
                'D': None,
                }

        self.sql_update()

    def legal_moves(self, current, player=None):  # this function returns all the squres with current access to them
        output = [(20, 20)]
        (x, y) = current
        if player is not None and self.state != player + 't':
            return output

        # moving up

        if x - 2 >= 0 and self.table[x - 1][y] == 'O':
            if self.table[x - 2][y] == 'O':
                output.append((x - 2, y))
            elif x - 4 >= 0 and self.table[x - 3][y] == 'O' \
                and self.table[x - 4][y] == 'O':
                output.append((x - 4, y))
            else:
                if y - 2 >= 0 and self.table[x - 2][y - 1] == 'O' \
                    and self.table[x - 2][y - 2] == 'O':
                    output.append((x - 2, y - 2))
                if y + 2 < 17 and self.table[x - 2][y + 1] == 'O' \
                    and self.table[x - 2][y + 2] == 'O':
                    output.append((x - 2, y + 2))

        # moving left

        if y - 2 >= 0 and self.table[x][y - 1] == 'O':
            if self.table[x][y - 2] == 'O':
                output.append((x, y - 2))
            elif y - 4 >= 0 and self.table[x][y - 3] == 'O' \
                and self.table[x][y - 4] == 'O':
                output.append((x, y - 4))
            else:
                if x - 2 >= 0 and self.table[x - 1][y - 2] == 'O' \
                    and self.table[x - 2][y - 2] == 'O':
                    output.append((x - 2, y - 2))
                if x + 2 < 17 and self.table[x + 1][y - 2] == 'O' \
                    and self.table[x + 2][y - 2] == 'O':
                    output.append((x + 2, y - 2))

        # moving down

        if x + 2 < 17 and self.table[x + 1][y] == 'O':
            if self.table[x + 2][y] == 'O':
                output.append((x + 2, y))
            elif x + 4 < 17 and self.table[x + 3][y] == 'O' \
                and self.table[x + 4][y] == 'O':
                output.append((x + 4, y))
            else:
                if y - 2 >= 0 and self.table[x + 2][y - 1] == 'O' \
                    and self.table[x + 2][y - 2] == 'O':
                    output.append((x + 2, y - 2))
                if y + 2 < 17 and self.table[x + 2][y + 1] == 'O' \
                    and self.table[x + 2][y + 2] == 'O':
                    output.append((x + 2, y + 2))

        # moving right

        if y + 2 < 17 and self.table[x][y + 1] == 'O':
            if self.table[x][y + 2] == 'O':
                output.append((x, y + 2))
            elif y + 4 < 17 and self.table[x][y + 3] == 'O' \
                and self.table[x][y + 4] == 'O':
                output.append((x, y + 4))
            else:
                if x - 2 >= 0 and self.table[x - 1][y + 2] == 'O' \
                    and self.table[x - 2][y + 2] == 'O':
                    output.append((x - 2, y + 2))
                if x + 2 < 17 and self.table[x + 1][y + 2] == 'O' \
                    and self.table[x + 2][y + 2] == 'O':
                    output.append((x + 2, y + 2))
        return output

    def move_pawn(self, player, new_pos):  # this function moves a pawn to position new_pos
        new_pos = (int(new_pos[0]), int(new_pos[1]))

        # checks if the move is legal or no

        if new_pos not in self.legal_moves(self.pawns_pos[player]):
            return "It's not your turn or this move is not legal."

        # moves the pawn and advances the turn by one

        self.table[self.pawns_pos[player][0]][self.pawns_pos[player][1]] = \
            'O'
        self.pawns_pos[player] = new_pos
        self.table[self.pawns_pos[player][0]][self.pawns_pos[player][1]] = \
            player
        self.state = chr(ord('A') + (ord(player) - ord('A') + 1)
                         % self.players_qty) + 't'

        # checks if someone won the game

        if self.players_qty == 2:
            for i in 'AB':
                if tuple(self.players_goal[player]) == (-1,
                        self.pawns_pos[player][1]) \
                    or tuple(self.players_goal[player]) \
                    == (self.pawns_pos[player][0], -1):
                    self.state = player + 'w'
                    user = \
                        tables.user.query.filter_by(username=self.players_name[player]).first()
                    user.score += 42
                    db.session.commit()
                    break
        elif self.players_qty == 4:
            for i in 'ABCD':
                if tuple(self.players_goal[player]) == (-1,
                        self.pawns_pos[player][1]) \
                    or tuple(self.players_goal[player]) \
                    == (self.pawns_pos[player][0], -1):
                    self.state = player + 'w'
                    user = \
                        tables.user.query.filter_by(username=self.players_name[player]).first()
                    user.score += 42
                    db.session.commit()
                    break

        # updates the game room properties

        self.sql_update()
        return '1'

    def check_walls(  # this fuction checks only for banning walls
        self,
        goal,
        current,
        colored_table=None,
        ):

        if colored_table is None:
            colored_table = []
            for _ in range(17):
                colored_table.append([])
                for _ in range(17):
                    colored_table[-1].append(0)
        colored_table[current[0]][current[1]] = 1
        if goal[0] == -1 and goal[1] == current[1] or goal[0] \
            == current[0] and goal[1] == -1:
            return True
        new_squares = self.legal_moves(current)
        for square in new_squares:
            if square[0] != 20 and square[1] != 20 \
                and colored_table[square[0]][square[1]] == 0 \
                and self.check_walls(goal, square, colored_table):
                return True
        return False

    def check_wall(  # this fuction checks if adding a wall is legal (checks all rules including banning walls)
        self,
        player,
        wall_pos,
        direction,
        ):

        (x, y) = (int(wall_pos[0]), int(wall_pos[1]))

        if direction == '-':
            if y - 1 < 0 or y + 1 >= 17 or self.table[x][y - 1] != 'O' \
                or self.table[x][y] != 'O' or self.table[x][y + 1] \
                != 'O':
                return False
            self.table[x][y - 1] = self.table[x][y] = self.table[x][y
                    + 1] = 'X'

            result = True
            if self.players_qty == 2:
                for i in 'AB':
                    result = result \
                        and self.check_walls(self.players_goal[i],
                            self.players_goal[i])
            if self.players_qty == 4:
                for i in 'ABCD':
                    result = result \
                        and self.check_walls(self.players_goal[i],
                            self.players_goal[i])

            self.table[x][y - 1] = self.table[x][y] = self.table[x][y
                    + 1] = 'O'

            return result
        elif direction == '|':

            if x - 1 < 0 or x + 1 >= 17 or self.table[x - 1][y] != 'O' \
                or self.table[x][y] != 'O' or self.table[x + 1][y] \
                != 'O':
                return False
            self.table[x - 1][y] = self.table[x][y] = self.table[x
                    + 1][y] = 'X'

            result = True
            if self.players_qty == 2:
                for i in 'AB':
                    result = result \
                        and self.check_walls(self.players_goal[i],
                            self.players_goal[i])
            if self.players_qty == 4:
                for i in 'ABCD':
                    result = result \
                        and self.check_walls(self.players_goal[i],
                            self.players_goal[i])

            self.table[x - 1][y] = self.table[x][y] = self.table[x
                    + 1][y] = 'O'

            return result

    def add_wall(  # this fuction adds a wall with center wall_pos in direction direction iff possible
        self,
        player,
        wall_pos,
        direction,
        ):

        (x, y) = (int(wall_pos[0]), int(wall_pos[1]))

        # checks if adding the wall is legal or no

        if self.players_walls[player] < 1:
            return 'You are out of walls.'
        if self.state != player + 't':
            return "It's not your turn."
        if not self.check_wall(player, wall_pos, direction):
            return 'This move is not legal.'

        # adds the wall and advances the turn by one and decrease the number of player walls by one

        if direction == '-':
            self.table[x][y - 1] = self.table[x][y] = self.table[x][y
                    + 1] = 'X'
        if direction == '|':
            self.table[x - 1][y] = self.table[x][y] = self.table[x
                    + 1][y] = 'X'
        self.state = chr(ord('A') + (ord(player) - ord('A') + 1)
                         % self.players_qty) + 't'
        self.players_walls[player] -= 1

        # updates the game room properties

        self.sql_update()
        return '1'

