from flask import Flask
from flask import request
from flask import session
import json
from server import app
from server import db
import tables
import quoridor

# create the database file if does not exist and create the tables and stuff

db.create_all()


# register page. this function proccesses register requests coming from clients

@app.route('/register', methods=['GET', 'POST'])
def register():

    # checks if the methods used to send values is post

    if request.method != 'POST':
        return 'Please send your information using post method.'

    # grabs postetd values

    username = request.form['username'].lower()
    password = request.form['password']

    # checks if posted values are empty

    if not username:
        return 'Please Enter a username.'
    if not password:
        return 'Please Enter a password.'

    # checks if posted username is already taken

    users = tables.user.query.filter_by(username=username).first()
    if users is not None:
        return 'This username already exists. Try another.'

    # adds a user to the sql database

    user = tables.user(username=username, password=password, score='0')
    db.session.add(user)
    db.session.commit()

    return '1'  # string '1' means requested operation successfully operated. it's a protcol between server application and client application.


# login page. this function proccesses login requests coming from clients

@app.route('/login', methods=['GET', 'POST'])
def login():

    # checks if the methods used to send values is post

    if request.method != 'POST':
        return 'Please send your login information using post method.'

    # grabs postetd values

    username = request.form['username'].lower()
    password = request.form['password']

    # checks if posted values are empty

    if not username:
        return 'Please Enter a username.'
    if not password:
        return 'Please Enter a password.'

    # checks if the username is valid

    user = tables.user.query.filter_by(username=username).first()
    if user is None:
        return 'username not found.'

    # checks if the password is valid

    if password != user.password:
        return 'invalid password'

    # sets the username key in session dict to username. actually logs the player in.

    session['username'] = username
    return '1'  # the same as first return '1' in this file


# logout page. this function proccesses logout requests coming from clients

@app.route('/logout', methods=['GET', 'POST'])
def logout():

    # unsets the username key in session dict. actually logs the player out.

    session.pop('username', None)
    return '1'  # the same as first return '1' in this file


# do page. this function proccesses all the operations (or actions) thoese are only possible to do while logged in

@app.route('/do', methods=['GET', 'POST'])
def do():

    # WEB-APP ACTIONS:

    # checks if the methods used to send values is post

    if request.method != 'POST':
        return 'Please send your information using post method.'

    # checks if the client is logged in

    if 'username' not in session:
        return 'No action is possible while not logged in.'

    # grabs the requested action to do

    action = request.form['action']

    if False:
        pass
    elif action == 'add_room':

        # this action adds a new room to rooms table in sql database

        room_name = request.form['room_name'].lower()
        game_type = request.form['game_type']
        owner = session['username']
        room = quoridor.GameRoom(room_name, new=True)
        return room.sql_add(game_type, owner)
    elif action == 'del_room':

        # this action removes the specified room from sql database

        room_name = request.form['room_name']
        current_user = session['username']
        room = quoridor.GameRoom(room_name)
        return room.sql_del(current_user)
    elif action == 'join_room':

        # this action adds the specified player to the specified room

        room_name = request.form['room_name']
        player = session['username']
        room = quoridor.GameRoom(room_name)
        return room.sql_join(player)
    elif action == 'leave_room':

        # this action removes the specified player from the specified room

        room_name = request.form['room_name']
        player = session['username']
        room = quoridor.GameRoom(room_name)
        return room.sql_leave(player)
    elif action == 'get_room_status':

        # this action returns all needed properties of the specified room to the client

        room_name = request.form['room_name']
        return quoridor.GameRoom(room_name).sql_get_json()
    elif action == 'get_rooms_list':

        # this action returns the list of all available rooms to the client

        return str(tables.room.query.all())
    elif action == 'get_users_list':

        # this action returns the list of all registered users and their scores to the client

        return str(tables.user.query.all())
    elif action == 'get_current_user':

        # this action returns the username of client user to itself

        return session['username']

    # GAME ACTIONS:

    elif action == 'get_legal_moves':


        # this action returns the position of all legal moves of specified player

        room_name = request.form['room_name']
        room = quoridor.GameRoom(room_name)
        player = room.real_to_player(session['username'])
        current = tuple(room.pawns_pos[player])
        return json.JSONEncoder().encode(room.legal_moves(current,
                player))
    elif action == 'move_pawn':

        # this action moves specified player's pawn to specified position if its legal and returns the result

        room_name = request.form['room_name']
        room = quoridor.GameRoom(room_name)
        x = request.form['x']
        y = request.form['y']
        player = room.real_to_player(session['username'])
        return room.move_pawn(player, (x, y))
    elif action == 'add_wall':

        # this action adds a wall for specified player if its legal and returns the result

        room_name = request.form['room_name']
        room = quoridor.GameRoom(room_name)
        x = request.form['x']
        y = request.form['y']
        direction = request.form['direction']
        player = room.real_to_player(session['username'])
        return room.add_wall(player, (x, y), direction)

    return 'Action not found!'  # if action was none of the above, this string will be returned

app.secret_key = 'K mirzavaziri IS very SECRETER than chars: !@#?'

